var numArr = [];

function themSo() {
  var number = document.getElementById("txt-number").value * 1;
  //   Reset ô input sau khi user nhập
  document.getElementById("txt-number").value = "";

  var addedNum = document.getElementById("array-result");

  //   Thêm phần tử vào mảng
  numArr.push(number);

  addedNum.innerHTML = "Mảng hiện tại: " + numArr;

  //
  sumPositiveNum();
  countPositiveNum();
  findMin();
  findMinPositiveNum();
  findLastEvenNumb();
  showPrime();
}

//   Tính Tổng Số Dương
function sumPositiveNum() {
  sumPositiveNumber = 0;
  numArr.forEach(function (number) {
    if (number > 0) {
      sumPositiveNumber += number;
    }
  });
  document.getElementById("sum-positive-result").innerText =
    "Tổng Số Dương: " + sumPositiveNumber;
}

// Đếm số dương
function countPositiveNum() {
  count = 0;
  positiveArr = numArr.filter(function (number) {
    return number > 0;
  });
  count = positiveArr.length;
  document.getElementById("count-positive").innerText =
    "Số Dương: " + count + " số";
}

// Tìm số nhỏ nhất
function findMin() {
  let min = numArr[0];
  for (let index = 0; index < numArr.length; index++) {
    let currentNumb = numArr[index];
    if (currentNumb < min) {
      min = currentNumb;
    }
  }
  document.getElementById("min-number").innerText = "Số nhỏ nhất là: " + min;
}

// Tìm số dương nhỏ nhất
function findMinPositiveNum() {
  let newPositiveArr = Array.from(positiveArr);
  let minPositive = newPositiveArr[0];
  for (let index = 0; index < newPositiveArr.length; index++) {
    let currentNumb = newPositiveArr[index];
    if (currentNumb < minPositive) {
      minPositive = currentNumb;
    }
  }
  document.getElementById("min-positive-number").innerText =
    "Số dương nhỏ nhất là: " + minPositive;
}

// Tìm số chẵn cuối cùng
function findLastEvenNumb() {
  evenNumbArr = numArr.filter(function (number) {
    return number % 2 == 0;
  });
  let newEvenNumbArr = Array.from(evenNumbArr);
  if (newEvenNumbArr.length > 0) {
    document.getElementById("last-even-number").innerText =
      "Số chẵn cuối cùng là: " + newEvenNumbArr[newEvenNumbArr.length - 1];
  } else {
    document.getElementById("last-even-number").innerText = "Không có số chẵn";
  }
}
// Đổi vị trí
function doiViTri() {
  var num1_index = document.getElementById("txt-number-1").value * 1 - 1;
  var num2_index = document.getElementById("txt-number-2").value * 1 - 1;
  var temp = 0;
  temp = numArr[num1_index];
  numArr[num1_index] = numArr[num2_index];
  numArr[num2_index] = temp;
  document.getElementById("new-array").innerHTML = "Mảng mới: " + numArr;
}

// Sắp xếp theo thứ tự tăng dần

function arrangeNumber() {
  numArr.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("increase-array").innerHTML =
    "Sau khi sắp xếp : " + numArr;
  // var temp = 0;
  // for (var index = 0; index < numArr.length; index++) {
  //   if (numArr[index] > numArr[index + 1]) {
  //     temp = numArr[index];
  //     numArr[index] = numArr[index + 1];
  //     numArr[index + 1] = temp;
  //   }
  // }
  // document.getElementById("increase-array").innerHTML =
  //   "Sau khi sắp xếp : " + numArr;
}

// Tìm Số Nguyên Tố Đầu Tiên

function isPrime(n) {
  let flag = 1;
  //flag = 0 => không phải số nguyên tố
  //flag = 1 => số nguyên tố

  if (n < 2)
    return (flag = 0); /*Số nhỏ hơn 2 không phải số nguyên tố => trả về 0*/

  /*Sử dụng vòng lặp while để kiểm tra có tồn tại ước số nào khác không*/
  let i = 2;
  while (i < n) {
    if (n % i == 0) {
      flag = 0;
      break; /*Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp*/
    }
    i++;
  }

  return flag;
}

function showPrime() {
  let primeResult = [];
  for (let i = 0; i < numArr.length; i++) {
    if (isPrime(numArr[i]) == 1) primeResult.push(numArr[i]);
  }

  if (primeResult.length > 0) {
    document.getElementById("first-prime-number").innerText =
      "Số nguyên tố đầu tiên là: " + primeResult[0];
  } else {
    document.getElementById("first-prime-number").innerText =
      "Không có số nguyên tố";
  }
}

// Thêm Mảng Số Thực

var realNumArr = [];
function themSoThuc() {
  var realNumber = document.getElementById("txt-real-number").value * 1;
  //   Reset ô input sau khi user nhập
  document.getElementById("txt-real-number").value = "";

  var addedRealNum = document.getElementById("real-num-result");

  //   Thêm phần tử vào mảng
  realNumArr.push(realNumber);

  addedRealNum.innerHTML = "Mảng số thực: " + realNumArr;
  countInteger();
  compareNumber();
}

// Đếm số nguyên
function countInteger() {
  let count = 0;
  realNumArr.forEach(function (number) {
    if (Number.isInteger(number)) {
      count++;
    }
  });
  document.getElementById("integer-result").innerText =
    "Số số thực: " + count + " số";
}

// So sánh số lượng số dương & âm

function compareNumber() {
  let compareResult = document.getElementById("compare-result");
  let countPositive = 0;
  let countNegative = 0;
  realNumArr.forEach(function (number) {
    if (number > 0) {
      countPositive++;
    }
    if (number < 0) {
      countNegative++;
    }
  });

  if (countPositive > countNegative) {
    compareResult.innerText = `Số dương: ${countPositive} - số âm: ${countNegative} - số dương nhiều hơn số âm`;
  }
  if (countPositive < countNegative) {
    compareResult.innerText = `Số dương: ${countPositive} - số âm: ${countNegative} - số dương ít hơn số âm`;
  }
  if (countPositive == countNegative) {
    compareResult.innerText = `Số dương: ${countPositive} - số âm: ${countNegative} - số dương bằng số âm`;
  }
}
